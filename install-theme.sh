#!/bin/bash
# Script written by Kris Springer
# https://www.krisspringer.com
# https://www.ionetworkadmin.com
#
# This script must have at least 755 permissions
################################################   
echo "Do you want this script to install files and overwrite your running xymonserver.cfg?"
select yn in "Yes" "No"; do
  case $yn in
    Yes ) 
echo "";
echo “Backing up existing files ...”; 
mv /usr/lib/xymon/server/www/menu/xymonmenu-grey.css /usr/lib/xymon/server/www/menu/xymonmenu-grey.css-old
mv /usr/lib/xymon/server/web/info_header /usr/lib/xymon/server/web/info_header-old
mv /usr/lib/xymon/server/web/maint_form /usr/lib/xymon/server/web/maint_form-old
mv /usr/lib/xymon/server/etc/xymonmenu.cfg /usr/lib/xymon/server/etc/xymonmenu.cfg-old
mv /usr/lib/xymon/server/etc/xymonserver.cfg /usr/lib/xymon/server/etc/xymonserver.cfg-old
echo "";
echo “Applying theme ...”; 
cp -r www/png-hex /usr/lib/xymon/server/www/
cp -r www/png-modern /usr/lib/xymon/server/www/
cp -r www/png-round /usr/lib/xymon/server/www/
cp www/menu/xymonmenu-grey.css /usr/lib/xymon/server/www/menu/xymonmenu-grey.css
cp web/info_header /usr/lib/xymon/server/web/info_header
cp web/maint_form /usr/lib/xymon/server/web/maint_form
cp etc/xymonmenu.cfg /usr/lib/xymon/server/etc/xymonmenu.cfg
cp etc/xymonserver.cfg /usr/lib/xymon/server/etc/xymonserver.cfg
echo "";
echo “Restarting Xymon Service ...”; 
service xymon restart
echo "";
echo “Finished. Refresh your Xymon webpage to see the new theme.”; 
echo "";
break;;
    No ) exit;;
  esac
done
