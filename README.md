## Description 
**Xymon Server Custom Theme**.  
The customized files and graphics included in this repo are for quickly theming Xymon Servers.  Although the default settings of Xymon work out of the box, they're ugly and lacking many interface options that make it professional. This theme overwrites your existing **xymonserver.cfg** file, so if you have any existing customizations you'll want to manually compare and modify to your liking before running the **install-theme** script below.  

The Hex theme is the default, but you have 2 other options to choose from.  You can view screenshots [here](https://gitlab.com/ionetworkadmin/xymon-server-custom-theme/-/tree/main/screenshots).

## Assumptions
1. You have installed the required Xymon Server packages.  
**`sudo apt install apache2 curl mrtg rrdtool librrds-perl git xymon`**
2. Xymon Server is v4.3.30 or later, and path it installed in is /usr/lib/xymon/server/  
3. You've configured your [Apache2 config](https://gitlab.com/ionetworkadmin/xymon-server-apache2-config) to work with Xymon.  
4. You've tested and have a functional default Xymon webpage.  
http://ServerIP/xymon/ 

## Instructions
**1.** SSH into your Xymon Server and navigate to it's working directory.  
**`cd /usr/lib/xymon/server/`**

**2.** Clone this repo.  
**`sudo git clone https://gitlab.com/ionetworkadmin/xymon-server-custom-theme.git custom-theme-sources`**

**3.** Navigate into new folder.    
**`cd custom-theme-sources`**

**4.** Edit the **xymonserver.cfg** file to define a few options specific to your environment and desired icon pack.  
If you skip this step it will load the Hex theme, which you can always change later.  

_**WARNING:**_ If you've already customized your existing xymonserver.cfg file you will need to cross reference those changes manually because the script in the next step is going to overwrite your running xymonserver.cfg file with this customized one. It does make a backup first though, so you can cross reference or revert later if needed.  

**`sudo nano etc/xymonserver.cfg`**  

Check these lines and edit as desired. Save when finished.  
```
# Theme selection. Uncomment the theme you want to use. Comment out the others.
XYMONSKIN="$XYMONSERVERWWWURL/png-hex"        # path for 'Hex' theme icons  
#XYMONSKIN="$XYMONSERVERWWWURL/png-modern"     # path for 'Modern' theme icons
#XYMONSKIN="$XYMONSERVERWWWURL/png-round"      # path for 'Round' theme icons  
```

**5.** Run the **install-theme.sh** script to automatically make all the required config edits.  
It will copy all the custom icons and configs into the appropriate locations.  
Backup copies of the originals are made automatically just in case you want to revert anything.  
All the files that get modified are listed in the script if you want to see what it's going to do before you run it.  

To run the script and perform the actions.  
**`sudo chmod 755 install-theme.sh`**  
**`sudo ./install-theme.sh`**

**6.** Refresh your Xymon webpage to see the new theme.

**7.** Feel free to delete the **custom-theme-sources** folder if you want. It's no longer needed.

## License
Open Source

## Authors and acknowledgment
Kris Springer  
https://www.ionetworkadmin.com 
